/*
 * Copyright (C) 2021 julia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package labs.otros;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.util.Locale;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ResourceBundle;
import java.text.MessageFormat;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
//import java.net.http

/**
 *
 * @author julia
 */
public class Ejemplos {
    
    enum Tipo{
        CASA, TORRE("12"), MIN("15");
        
        private String l;
        
        private Tipo(){
            
        }
        
        private Tipo(String l){
            this.l = l;
        }
        
        private void setL(String l){
            this.l = l;
        }
        
        private String getL(){
            return l;
        }
    };

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        try {
            Tipo.CASA.setL("si puede");
            for(Tipo t : Tipo.values()){
                System.out.println("T "+t.getL());
            }
            
            dates();
            files();
            functions();
            collections();
            threads();
        } catch (IOException ex) {
            Logger.getLogger(Ejemplos.class.getName()).log(Level.WARNING, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(Ejemplos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private static void threads() throws InterruptedException, ExecutionException, TimeoutException{
        System.out.println("ThNameA:"+Thread.currentThread().getName());
        
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        executorService.execute(() -> { //Runable
            System.out.println("ThNameB:"+Thread.currentThread().getName());
            System.out.println("Imprimiendo en runable");
        });
        Future<String> call = executorService.submit(()-> { //Callabel
            System.out.println("ThNameC:"+Thread.currentThread().getName());
           return "Algo da"; 
        });
        
        
        executorService.shutdown();
        
        Future<String> algoF = new FutureTask<>(new Callable<String>() {
            @Override
            public String call() throws Exception {
                System.out.println("ThNameD:"+Thread.currentThread().getName());
                return "Llamada del futuro?";
            }
        });                
        System.out.println("call:"+call.get());
        //System.out.println("F2 "+algoF.get(2, TimeUnit.SECONDS));
        
    }

    private static void collections() {
        Function<String, String> swap = s -> {
            if (s.equals("Australia")) {
                return "New Zealand";
            } else {
                return s;
            }
        };

        Set<String> islandNations = Set.of("Australia", "Japan", "Taiwan", "Cyprus", "Cuba");
        islandNations = islandNations.stream()
                .collect(Collectors.toSet());
        for (String s : islandNations) {
            System.out.print(s);
        }
        
        System.out.println("-----------------------------------");
        List<String> algo = List.of("Uno", "Dos", "Tres");
        List<String> algo2 = List.of("Unoy", "Dosy", "Tres2y", "Dadoy", "Docey");
        System.out.println("equals lists: "+algo.equals(algo2));
        
        X:
        for (int i = 0; i < algo.size(); i++) {
            String a = algo.get(i);
            System.out.println("A: "+a);
            Y:
            for (int j = 0; j < algo2.size(); j++) {
                String b = algo2.get(j);
                if(b.equals("Dosy")){
                    break Y;
                }
                if(b.equals("Dadoy")){
                    continue X;
                }
                System.out.println("B: "+b);
            }
        }
        System.out.println("-----------------------------------");
        
        String reduce = algo2.stream().parallel()
                .filter(p -> p.length()==3)
                .reduce((s1, s2) -> s1+" "+s2)
                .orElseThrow();
        System.out.println("Reduce: "+reduce);
        
        Map<Integer, Set<String>> grupos = algo2.stream().parallel()
                 .collect(Collectors.groupingBy(x -> x.length(), Collectors.filtering(x -> x.startsWith("D"), Collectors.toSet())));
        grupos.forEach((s, set) -> {
            System.out.println("Grupo: "+s);
            System.out.println("set: "+set);
        });
        
        Stream<Integer> numeros = Stream.generate(() -> (int)Math.random()*10).takeWhile(n -> n!= 3);
    }

    private static void functions() {
        Predicate<String> stringFilter = (s) -> s.equals("Ana");
        Function<String, String> stringFunct = (s2) -> {
            return s2.toUpperCase();
        };
        UnaryOperator<String> stringUnary = (s3) -> s3.toLowerCase();
        Consumer<String> stringConsumer = (s4) -> System.out.println(MessageFormat.format("Es {0}", s4));
        Supplier<String> stringSupplier = () -> String.valueOf("1.5");
        //stringFilter.or
        

        String[] nombres = {"Ana", "Felixa"};
        Arrays.stream(nombres)
                .filter(n -> n.startsWith("A")).forEach(n2 -> n2.replace("a", "z"));
        //System.out.println("Nombres: "+nombres);
        Arrays.stream(nombres)
                //.generate(stringSupplier)
                .filter(stringFilter)
                .peek(stringConsumer)
                .map(stringUnary)
                .collect(Collectors.averagingLong(c -> c.length()));
        //.reduce((s1, s2) -> s1+" "+s2);
        //.forEach(stringConsumer);

        BiPredicate<String, Integer> biString = (s, i) -> s.length() == i.intValue();

        Optional<String> primero = Arrays.stream(nombres)
                .findFirst();
        System.out.println("Primero: " + primero.orElse("Nada"));
    }

    private static void files() throws IOException {
        Path someFile = Path.of("/", "users", "joe", "docs", "some.txt");
        System.out.println("1: " + someFile);
        System.out.println("2: " + someFile.getFileName());
        System.out.println("3: " + someFile.getParent());
        System.out.println("4: " + Path.of("."));
        System.out.println("5: " + someFile.getParent().resolve("../pics/acme.jpg"));
        System.out.println("6: " + someFile.getFileName().resolveSibling("other.txt"));
        System.out.println("7: " + someFile.getParent().resolve("../pics/acme.jpg").normalize());
        System.out.println("8: " + someFile.relativize(someFile.getParent().resolve("../pics/acme.jpg")));
        //System.out.println("9: " + someFile.getParent().resolve("../pics/acme.jpg").toRealPath(LinkOption.NOFOLLOW_LINKS));

        Files.list(Path.of(".")).forEach(file -> { //walk
            System.out.println("File: " + file);
            //Files.copy(file, out)
        });

        //HttpRequest client;
    }

    private static void dates() {
        System.out.println("Instant: " + Instant.now());
        BigDecimal price = BigDecimal.valueOf(1.85);
        BigDecimal rate = BigDecimal.valueOf(0.065);

        price = price.subtract(price.multiply(rate)).setScale(2, RoundingMode.HALF_UP);

        Locale locale = Locale.FRANCE;
        NumberFormat currencyFormat = NumberFormat.getCurrencyInstance(locale);
        NumberFormat percentFormat = NumberFormat.getPercentInstance(locale);
        percentFormat.setMaximumFractionDigits(2);
        currencyFormat.format(price);
        percentFormat.format(rate);

        locale = Locale.forLanguageTag("en-GB");
        currencyFormat = NumberFormat.getCurrencyInstance(locale);
        currencyFormat.format(price);

        LocalDate today = LocalDate.now();
        today.plusYears(1).getDayOfWeek();
        LocalTime teaTime = LocalTime.of(17, 30);
        Duration timeGap = Duration.between(LocalTime.now(), teaTime);

        Duration.between(LocalTime.now(), LocalTime.of(0, 2));
        timeGap.toMinutes();
        timeGap.toHours();
        timeGap.toMinutes();
        timeGap.toMinutesPart();

        LocalDateTime tomorrowTeaTime = LocalDateTime.of(today.plusDays(1), teaTime);

        ZoneId london = ZoneId.of("Europe/London");
        ZoneId katmandu = ZoneId.of("Asia/Katmandu");
        ZonedDateTime londonTime = ZonedDateTime.of(tomorrowTeaTime, london);
        ZonedDateTime katmanduTime = londonTime.withZoneSameInstant(katmandu);
        System.out.println("" + katmanduTime.getOffset());

        String datePattern = "EE', 'd' of 'MMMM yyyy' at 'HH:mm z";

        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern(datePattern, locale);
        locale = Locale.UK;
        dateFormat = DateTimeFormatter.ofPattern(datePattern, locale);
        String timeTxt = dateFormat.format(katmanduTime);
        locale = new Locale("es", "CO");
        dateFormat = DateTimeFormatter.ofPattern(datePattern, locale);
        timeTxt = dateFormat.format(katmanduTime);
        System.out.println("Time: " + timeTxt);

        DateTimeFormatter dateForma = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).localizedBy(locale);
        System.out.println("Date: " + dateForma.format(today));
    }

    public void textExceptions() {
        try {
            Algo algo = new Algo();
            algo.metodo();
            algo.metod2();
        } catch (L | M ex) {
            ex.printStackTrace();
        }
    }

    public interface Pet {

        public abstract int breed();
        // line n2

        public default Pet callPet() {
            return this;
        }
        // line n3

        @Deprecated(forRemoval = true)
        void speak();
    }

    public class M extends Exception {

    }

    public class L extends RuntimeException {

    }

    public class N extends M {

    }

    public class Algo {

        public void metodo() throws M {
            throw new N();
        }

        public void metod2() throws L {

        }

    }
    
    public class Product{
        protected String nombre;        

        public String getNombre() {
            return nombre;
        }
    }
    
    public class Manzana extends Product{
        
    }
    
    public class Pera extends Product{
        
    }
    
    public class WildCard <T extends Product>{
        
        private T product;
        
        public void filtrar(WildCard<?> pro){
            product.getNombre().equals(pro.product.getNombre());
        }
    }
}
