/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labs.pm.data;

/**
 * Interfaz para calificar los productos.<br>
 * FunctionalInterface es opcional, nos ayuda a evitar tener mas de un metodo abstracto
 * @author julia
 */
@FunctionalInterface
public interface Rateable<T> {
    
    public static final Rating DEFAULT_RATING = Rating.NO_RATED;
    
    /**
     * Se puede quitar public y abstract porque esta implicito en las interfaces
     * @param rating
     * @return 
     */
    public abstract T applyRating(Rating rating);
    
    /**
     * 
     * @return 
     */
    public default Rating getRating() {
        return DEFAULT_RATING;
    }
    
    /**
     * 
     * @param stars
     * @return 
     */
    public static Rating convert(int stars){
        return (stars>=0 && stars<=5) ? Rating.values()[stars] : DEFAULT_RATING;
    }
    
    /**
     * 
     * @param stars
     * @return 
     */
    public default T applyRating(int stars){
        return applyRating(convert(stars));
    }

    
}
